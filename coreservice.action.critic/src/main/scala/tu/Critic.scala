package tu

/**
 * Critic trait.
 * @author toscheva
 * Date: 03.05.12
 * Time: 11:05
 */

trait Critic {

  def excludesLinks();

  def includesLinks();

  //TODO: add classes to knowledge project
  //def apply(context:Context)
}
