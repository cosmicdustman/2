# Thinking level design specification.

Thinking levels concept based on the [Critic Selector Model of Mind](http://web.media.mit.edu/~minsky/E7/eb7.html#_Toc508708572) by Marvin Minsky.

![Thinking model](http://web.media.mit.edu/~minsky/E7/eb7_files/image003.png).

Each [process](process.md) is placed in proper level [KLine](knowledge.md#KLine) according to level of main [Critic](critic.md), by [ThinkingLifeCycle](thinking-life-cycle.md).
Each upper level controls and manages processes of lover level.
