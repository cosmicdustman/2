package tu.model.knowledge

/**
 * @author max
 * date 2012-04-29
 * Time: 11:34 PM
 */

class Probability(_frequency: Double = 0.0, _confidence: Double = 1.0) {

  def frequency: Double = _frequency
  def confidence: Double = _confidence

}
